<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNossoTrabalhoTable extends Migration
{
    public function up()
    {
        Schema::create('nosso_trabalho', function (Blueprint $table) {
            $table->increments('id');
            $table->text('abertura');
            $table->text('arquitetura');
            $table->text('design_de_interiores');
            $table->text('gestao_de_obra');
            $table->text('consultoria');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('nosso_trabalho');
    }
}
