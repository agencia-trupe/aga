<?php

use Illuminate\Database\Seeder;

class NossoTrabalhoSeeder extends Seeder
{
    public function run()
    {
        DB::table('nosso_trabalho')->insert([
            'abertura' => '',
            'arquitetura' => '',
            'design_de_interiores' => '',
            'gestao_de_obra' => '',
            'consultoria' => '',
        ]);
    }
}
