<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class NossoTrabalhoRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'abertura' => 'required',
            'arquitetura' => 'required',
            'design_de_interiores' => 'required',
            'gestao_de_obra' => 'required',
            'consultoria' => 'required',
        ];
    }
}
