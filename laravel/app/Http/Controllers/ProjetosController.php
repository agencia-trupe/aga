<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Projeto;

class ProjetosController extends Controller
{
    public function index(Projeto $projeto)
    {
        if ($projeto->exists) {
            return view('frontend.projetos.show', compact('projeto'));
        }

        $projetos = Projeto::ordenados()->get();

        return view('frontend.projetos.index', compact('projetos'));
    }
}
