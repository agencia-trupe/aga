<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\NossoTrabalho;

class NossoTrabalhoController extends Controller
{
    public function index()
    {
        $nossoTrabalho = NossoTrabalho::first();

        return view('frontend.nosso-trabalho', compact('nossoTrabalho'));
    }
}
