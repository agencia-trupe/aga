<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\NossoTrabalhoRequest;
use App\Http\Controllers\Controller;

use App\Models\NossoTrabalho;

class NossoTrabalhoController extends Controller
{
    public function index()
    {
        $registro = NossoTrabalho::first();

        return view('painel.nosso-trabalho.edit', compact('registro'));
    }

    public function update(NossoTrabalhoRequest $request, NossoTrabalho $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.nosso-trabalho.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
