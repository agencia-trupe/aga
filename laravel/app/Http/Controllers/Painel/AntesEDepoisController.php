<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\AntesEDepoisRequest;
use App\Http\Controllers\Controller;

use App\Models\AntesEDepois;

class AntesEDepoisController extends Controller
{
    public function index()
    {
        $registros = AntesEDepois::ordenados()->get();

        return view('painel.antes-e-depois.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.antes-e-depois.create');
    }

    public function store(AntesEDepoisRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['antes'])) $input['antes'] = AntesEDepois::upload_antes($input['orientacao']);

            if (isset($input['depois'])) $input['depois'] = AntesEDepois::upload_depois($input['orientacao']);

            AntesEDepois::create($input);

            return redirect()->route('painel.antes-e-depois.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(AntesEDepois $registro)
    {
        return view('painel.antes-e-depois.edit', compact('registro'));
    }

    public function update(AntesEDepoisRequest $request, AntesEDepois $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['antes'])) $input['antes'] = AntesEDepois::upload_antes($input['orientacao']);

            if (isset($input['depois'])) $input['depois'] = AntesEDepois::upload_depois($input['orientacao']);

            $registro->update($input);

            return redirect()->route('painel.antes-e-depois.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(AntesEDepois $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.antes-e-depois.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
