<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\AntesEDepois;

class AntesEDepoisController extends Controller
{
    public function index()
    {
        $antesEDepois = AntesEDepois::ordenados()->get();

        return view('frontend.antes-e-depois', compact('antesEDepois'));
    }
}
