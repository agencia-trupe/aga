<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class NossoTrabalho extends Model
{
    protected $table = 'nosso_trabalho';

    protected $guarded = ['id'];

}
