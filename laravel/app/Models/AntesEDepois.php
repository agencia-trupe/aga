<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class AntesEDepois extends Model
{
    protected $table = 'antes_e_depois';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_antes($orientacao)
    {
        if ($orientacao === 'horizontal') {
            return CropImage::make('antes', [
                [
                    'width'  => 425,
                    'height' => 365,
                    'path'   => 'assets/img/antes-e-depois/thumbs/'
                ],
                [
                    'width'  => 700,
                    'height' => 500,
                    'path'   => 'assets/img/antes-e-depois/'
                ],
            ]);
        } else {
            return CropImage::make('antes', [
                [
                    'width'  => 210,
                    'height' => 365,
                    'path'   => 'assets/img/antes-e-depois/thumbs/'
                ],
                [
                    'width'  => 500,
                    'height' => 700,
                    'path'   => 'assets/img/antes-e-depois/'
                ],
            ]);
        }
    }

    public static function upload_depois($orientacao)
    {
        if ($orientacao === 'horizontal') {
            return CropImage::make('depois', [
                [
                    'width'  => 425,
                    'height' => 365,
                    'path'   => 'assets/img/antes-e-depois/thumbs/'
                ],
                [
                    'width'  => 700,
                    'height' => 500,
                    'path'   => 'assets/img/antes-e-depois/'
                ],
            ]);
        } else {
            return CropImage::make('depois', [
                [
                    'width'  => 210,
                    'height' => 365,
                    'path'   => 'assets/img/antes-e-depois/thumbs/'
                ],
                [
                    'width'  => 500,
                    'height' => 700,
                    'path'   => 'assets/img/antes-e-depois/'
                ],
            ]);
        }
    }
}
