import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';

AjaxSetup();
MobileToggle();

$('.banners').cycle({
    slides: '.slide'
});

$('.projetos .imagens a').fancybox({
    padding: 0,
    helpers: {
        overlay: {
            css: { 'background': 'rgba(0, 0, 0, 0.8)' }
        }
    }
});

$('.antes-e-depois .thumb').click(function(event) {
    event.preventDefault();

    $('#popup-antes-e-depois').html($(this).find('.popup-content').html());

    var larguraMaxima = $(this).hasClass('thumb-horizontal') ? '1280px' : '940px';

    $.fancybox({
        href: '#popup-antes-e-depois',
        padding: 0,
        scrolling: 'no',
        autoSize:  false,
        autoResize:  false,
        width: '100%',
        height: 'auto',
        maxWidth: larguraMaxima,
        beforeShow: function () {
            $('.fancybox-skin').css({
                backgroundColor: 'transparent',
                boxShadow: 'none',
            });
        },
        helpers: {
            overlay: {
                css: { 'background': 'rgba(0, 0, 0, 0.8)' }
            }
        }
    });
});

$(document).on('submit', '#form-contato', function(event) {
    event.preventDefault();

    var $form     = $(this),
        $response = $('#form-contato-response');

    if ($form.hasClass('sending')) return false;

    $response.fadeOut('fast');
    $form.addClass('sending');

    $.ajax({
        type: "POST",
        url: $('base').attr('href') + '/contato',
        data: {
            nome: $('#nome').val(),
            email: $('#email').val(),
            telefone: $('#telefone').val(),
            mensagem: $('#mensagem').val(),
        },
        success: function(data) {
            $response.fadeOut().text(data.message).fadeIn('slow');
            $form[0].reset();
        },
        error: function(data) {
            var error = 'Preencha todos os campos corretamente.';
            $response.fadeOut().text(error).fadeIn('slow');
        },
        dataType: 'json'
    })
    .always(function() {
        $form.removeClass('sending');
    });
});
