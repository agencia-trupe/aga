@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Nosso Trabalho</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.nosso-trabalho.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.nosso-trabalho.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
