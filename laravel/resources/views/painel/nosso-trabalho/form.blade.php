@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('abertura', 'Abertura') !!}
    {!! Form::textarea('abertura', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('arquitetura', 'Arquitetura') !!}
    {!! Form::textarea('arquitetura', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('design_de_interiores', 'Design de Interiores') !!}
    {!! Form::textarea('design_de_interiores', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('gestao_de_obra', 'Gestão de Obra') !!}
    {!! Form::textarea('gestao_de_obra', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('consultoria', 'Consultoria') !!}
    {!! Form::textarea('consultoria', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
