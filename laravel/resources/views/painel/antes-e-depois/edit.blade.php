@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Antes e Depois /</small> Editar Antes e Depois</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.antes-e-depois.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.antes-e-depois.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
