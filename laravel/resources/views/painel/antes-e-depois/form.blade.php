@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('orientacao', 'Orientação') !!}
    {!! Form::select('orientacao', ['horizontal' => 'Horizontal', 'vertical' => 'Vertical'], null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('antes', 'Antes') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/antes-e-depois/'.$registro->antes) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('antes', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('depois', 'Depois') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/antes-e-depois/'.$registro->depois) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('depois', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.antes-e-depois.index') }}" class="btn btn-default btn-voltar">Voltar</a>
