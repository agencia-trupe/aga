@extends('frontend.common.template')

@section('content')

    <div class="sobre">
        <h1>SOBRE</h1>

        <div class="texto">
            @if($sobre->imagem)
            <img src="{{ asset('assets/img/sobre/'.$sobre->imagem) }}" alt="">
            @endif
            {!! $sobre->texto !!}
        </div>
    </div>

@endsection
