@extends('frontend.common.template')

@section('content')

    <div class="banners">
        @foreach($banners as $banner)
        <div class="slide" style="background-image:url({{ asset('assets/img/banners/'.$banner->imagem) }})"></div>
        @endforeach
    </div>

    <div class="home center">
        <div class="entrar">
            <a href="{{ route('nosso-trabalho') }}">
                <img src="{{ asset('assets/img/layout/aga-marca1.png') }}" alt="">
                <span>ENTRAR</span>
            </a>
        </div>
    </div>

@endsection
