@extends('frontend.common.template')

@section('content')

    <div class="nosso-trabalho">
        <h1>NOSSO TRABALHO</h1>

        <div class="texto">
            {!! $nossoTrabalho->abertura !!}
        </div>

        <div class="areas">
            <div>
                <div class="imagem">
                    <img src="{{ asset('assets/img/layout/arquitetura.png') }}" alt="">
                </div>
                ARQUITETURA
            </div>
            <div>
                <div class="imagem">
                    <img src="{{ asset('assets/img/layout/design-de-interiores.png') }}" alt="">
                </div>
                DESIGN DE INTERIORES
            </div>
            <div>
                <div class="imagem">
                    <img src="{{ asset('assets/img/layout/gestao-de-obras.png') }}" alt="">
                </div>
                GESTÃO DE OBRA
            </div>
            <div>
                <div class="imagem">
                    <img src="{{ asset('assets/img/layout/consultoria.png') }}" alt="">
                </div>
                CONSULTORIA
            </div>
        </div>

        <div class="texto">
            <h2>Arquitetura:</h2>
            {!! $nossoTrabalho->arquitetura !!}

            <h2>Design de Interiores:</h2>
            {!! $nossoTrabalho->design_de_interiores !!}

            <h2>Gestão de Obra:</h2>
            {!! $nossoTrabalho->gestao_de_obra !!}

            <h2>Consultoria:</h2>
            {!! $nossoTrabalho->consultoria !!}
        </div>
    </div>

@endsection
