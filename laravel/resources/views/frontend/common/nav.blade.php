<a href="{{ route('nosso-trabalho') }}" @if(Tools::isActive('nosso-trabalho')) class="active" @endif>NOSSO TRABALHO</a>
<a href="{{ route('projetos') }}" @if(Tools::isActive('projetos')) class="active" @endif>PROJETOS</a>
<a href="{{ route('antes-e-depois') }}" @if(Tools::isActive('antes-e-depois')) class="active" @endif>ANTES & DEPOIS</a>
<a href="{{ route('sobre') }}" @if(Tools::isActive('sobre')) class="active" @endif>SOBRE</a>
<a href="{{ route('contato') }}" @if(Tools::isActive('contato')) class="active" @endif>CONTATO</a>

<div class="social">
    @foreach(['instagram', 'facebook'] as $s)
        @if($contato->{$s})
        <a href="{{ $contato->{$s} }}" target="_blank">
            <img src="{{ asset('assets/img/layout/'.$s.'.png') }}" alt="">
        </a>
        @endif
    @endforeach
</div>
