<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="index, follow">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <base href="{{ url('/') }}">

    <meta name="author" content="Trupe Agência Criativa">
    <meta name="copyright" content="{{ date('Y') }} Trupe Agência Criativa">
    <meta name="description" content="{{ $config->description }}">
    <meta name="keywords" content="{{ $config->keywords }}">

    <meta property="og:title" content="{{ $config->title }}">
    <meta property="og:description" content="{{ $config->description }}">
    <meta property="og:site_name" content="{{ $config->title }}">
    <meta property="og:type" content="website">
    <meta property="og:url" content="{{ Request::url() }}">
@if($config->imagem_de_compartilhamento)
    <meta property="og:image" content="{{ asset('assets/img/'.$config->imagem_de_compartilhamento) }}">
@endif

    <title>{{ $config->title }}</title>

    {!! Tools::loadCss('css/vendor.main.css') !!}
    {!! Tools::loadCss('vendor/fancybox/source/jquery.fancybox.css') !!}
    {!! Tools::loadCss('css/main.css') !!}
</head>
<body>
@if(Route::currentRouteName() === 'home')
    @yield('content')
@else
    <div class="mobile">
        <div class="center">
            <img src="{{ asset('assets/img/layout/aga-marca2.png') }}" alt="">
            <button id="mobile-toggle" type="button" role="button">
                <span class="lines"></span>
            </button>
            <nav>
                @include('frontend.common.nav')
            </nav>
        </div>
    </div>

    <div class="center">
        <div class="wrapper">
            <aside>
                <img src="{{ asset('assets/img/layout/aga-marca2.png') }}" alt="">
                <nav>
                    @include('frontend.common.nav')
                </nav>
            </aside>
            <main>
                @yield('content')
            </main>
        </div>
    </div>

    <footer>
        <div class="center">
            <p>
                © {{ date('Y') }} {{ $config->nome_do_site }} &middot; Todos os direitos reservados <span>&middot;</span> <a href="http://www.trupe.net" target="_blank">Criação de Sites:</a> <a href="http://www.trupe.net" target="_blank">Trupe</a></p>
        </div>
    </footer>
@endif

    {!! Tools::loadJquery() !!}
    {!! Tools::loadJs('js/vendor.main.js') !!}
    {!! Tools::loadJs('vendor/fancybox/source/jquery.fancybox.pack.js') !!}
    {!! Tools::loadJs('js/main.js') !!}

@if($config->analytics)
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', '{{ $config->analytics }}', 'auto');
        ga('send', 'pageview');
    </script>
@endif
</body>
</html>
