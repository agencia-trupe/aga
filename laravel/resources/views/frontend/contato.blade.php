@extends('frontend.common.template')

@section('content')

    <div class="contato">
        <h1>CONTATO</h1>

        <div class="info">
            <img src="{{ asset('assets/img/layout/telefone.png') }}" alt="">
            <h2>TELEFONE</h2>
            <p>{{ $contato->telefone }}</p>

            <img src="{{ asset('assets/img/layout/localizacao.png') }}" alt="">
            <h2>ENDEREÇO</h2>
            <p>{!! $contato->endereco !!}</p>
        </div>

        <form action="" id="form-contato" method="POST">
            <input type="text" name="nome" id="nome" placeholder="Nome" required>
            <input type="email" name="email" id="email" placeholder="E-mail" required>
            <input type="text" name="telefone" id="telefone" placeholder="Telefone">
            <textarea name="mensagem" id="mensagem" placeholder="Mensagem" required></textarea>
            <input type="submit" value="ENVIAR">
            <div id="form-contato-response"></div>
        </form>
    </div>

@endsection
