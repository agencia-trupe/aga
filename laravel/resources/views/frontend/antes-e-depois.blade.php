@extends('frontend.common.template')

@section('content')

    <div class="antes-e-depois">
        <h1>ANTES & DEPOIS</h1>

        <div>
            @foreach($antesEDepois as $imagem)
            <a href="#" class="thumb thumb-{{ $imagem->orientacao }}">
                <div class="popup-content">
                    <img src="{{ asset('assets/img/antes-e-depois/'.$imagem->antes) }}" alt="">
                    <img src="{{ asset('assets/img/antes-e-depois/'.$imagem->depois) }}" alt="">
                </div>
                <div>
                    <img src="{{ asset('assets/img/antes-e-depois/thumbs/'.$imagem->antes) }}" alt="">
                    <div class="overlay">
                        <span>ANTES</span>
                    </div>
                </div>
                <div>
                    <img src="{{ asset('assets/img/antes-e-depois/thumbs/'.$imagem->depois) }}" alt="">
                    <div class="overlay">
                        <span>DEPOIS</span>
                    </div>
                </div>
            </a>
            @endforeach
        </div>
    </div>

    <div style="display:none">
        <div id="popup-antes-e-depois">

        </div>
    </div>

@endsection
