@extends('frontend.common.template')

@section('content')

    <div class="projetos">
        <h1>PROJETOS</h1>

        <div class="texto">
            <h2>{{ $projeto->titulo }}</h2>
            <p>{!! $projeto->descricao !!}</p>
        </div>

        <div class="imagens">
            @foreach($projeto->imagens->chunk(6) as $chunk)
            <?php
                $count  = 0;
                $pastas = [
                    'thumb-vertical',
                    'thumb-vertical',
                    'thumb-vertical',
                    'thumb-horizontal-sm',
                    'thumb-horizontal-sm',
                    'thumb-horizontal-lg'
                ];
            ?>
            <div class="chunk">
                @foreach($chunk as $imagem)
                @if($count == 3) <div class="group"> @endif
                <a href="{{ asset('assets/img/projetos/imagens/'.$imagem->imagem) }}" rel="projeto">
                    <img src="{{ asset('assets/img/projetos/imagens/'.$pastas[$count].'/'.$imagem->imagem) }}" alt="">
                </a>
                @if($count == 4) </div> @endif
                <?php $count++; ?>
                @endforeach
            </div>
            @endforeach
        </div>
    </div>

@endsection
