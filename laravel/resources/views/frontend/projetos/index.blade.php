@extends('frontend.common.template')

@section('content')

    <div class="projetos">
        <h1>PROJETOS</h1>

        <div>
            @foreach($projetos as $projeto)
            <a href="{{ route('projetos', $projeto->slug) }}" class="thumb">
                <div class="imagem">
                    <img src="{{ asset('assets/img/projetos/'.$projeto->capa) }}" alt="">
                </div>
                <div class="texto">
                    <h3>{{ $projeto->titulo }}</h3>
                    <p>{!! $projeto->descricao !!}</p>
                    <span>ver projeto</span>
                </div>
            </a>
            @endforeach
        </div>
    </div>

@endsection
