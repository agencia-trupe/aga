-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Tempo de geração: 09/02/2018 às 03:42
-- Versão do servidor: 8.0.2-dmr
-- Versão do PHP: 7.0.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `aga`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `antes_e_depois`
--

CREATE TABLE `antes_e_depois` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `orientacao` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `antes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `depois` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `antes_e_depois`
--

INSERT INTO `antes_e_depois` (`id`, `ordem`, `orientacao`, `antes`, `depois`, `created_at`, `updated_at`) VALUES
(1, 0, 'horizontal', '1_20180209033753.jpg', '2_20180209033754.jpg', '2018-02-09 03:37:54', '2018-02-09 03:37:54'),
(2, 0, 'vertical', '10_20180209033816.jpg', '11_20180209033817.jpg', '2018-02-09 03:38:17', '2018-02-09 03:38:17'),
(3, 0, 'vertical', '20_20180209034129.jpg', '21_20180209034130.jpg', '2018-02-09 03:41:30', '2018-02-09 03:41:30');

-- --------------------------------------------------------

--
-- Estrutura para tabela `banners`
--

CREATE TABLE `banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `banners`
--

INSERT INTO `banners` (`id`, `ordem`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 0, '6_20180209033556.jpg', '2018-02-09 03:35:56', '2018-02-09 03:35:56');

-- --------------------------------------------------------

--
-- Estrutura para tabela `configuracoes`
--

CREATE TABLE `configuracoes` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome_do_site` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem_de_compartilhamento` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `analytics` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `configuracoes`
--

INSERT INTO `configuracoes` (`id`, `nome_do_site`, `title`, `description`, `keywords`, `imagem_de_compartilhamento`, `analytics`, `created_at`, `updated_at`) VALUES
(1, 'AGA', 'AGA &middot; Adriana Gazzoni Akutsu', '', '', '', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `contato`
--

CREATE TABLE `contato` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `contato`
--

INSERT INTO `contato` (`id`, `email`, `telefone`, `endereco`, `instagram`, `facebook`, `created_at`, `updated_at`) VALUES
(1, 'contato@trupe.net', '+55 11 1234 5678', 'Rua Nome da Rua, 000 - Bairro<br />\r\nCidade - Estado', '#', '#', NULL, '2018-02-09 03:36:54');

-- --------------------------------------------------------

--
-- Estrutura para tabela `contatos_recebidos`
--

CREATE TABLE `contatos_recebidos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_02_01_000000_create_users_table', 1),
('2016_03_01_000000_create_contato_table', 1),
('2016_03_01_000000_create_contatos_recebidos_table', 1),
('2017_09_01_163723_create_configuracoes_table', 1),
('2018_02_07_185614_create_banners_table', 1),
('2018_02_07_190119_create_nosso_trabalho_table', 1),
('2018_02_07_190250_create_sobre_table', 1),
('2018_02_07_190956_create_antes_e_depois_table', 1),
('2018_02_08_131830_create_projetos_table', 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `nosso_trabalho`
--

CREATE TABLE `nosso_trabalho` (
  `id` int(10) UNSIGNED NOT NULL,
  `abertura` text COLLATE utf8_unicode_ci NOT NULL,
  `arquitetura` text COLLATE utf8_unicode_ci NOT NULL,
  `design_de_interiores` text COLLATE utf8_unicode_ci NOT NULL,
  `gestao_de_obra` text COLLATE utf8_unicode_ci NOT NULL,
  `consultoria` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `nosso_trabalho`
--

INSERT INTO `nosso_trabalho` (`id`, `abertura`, `arquitetura`, `design_de_interiores`, `gestao_de_obra`, `consultoria`, `created_at`, `updated_at`) VALUES
(1, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sit amet sem neque. In id urna quis erat tempus placerat ut quis nisl. Etiam imperdiet nunc luctus sem hendrerit, ac scelerisque diam cursus. Duis sodales pharetra nunc, sed semper lacus luctus a. Nullam eget fringilla ligula, vel feugiat diam.</p>\r\n', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sit amet sem neque. In id urna quis erat tempus placerat ut quis nisl. Etiam imperdiet nunc luctus sem hendrerit, ac scelerisque diam cursus. Duis sodales pharetra nunc, sed semper lacus luctus a. Nullam eget fringilla ligula, vel feugiat diam.</p>\r\n', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sit amet sem neque. In id urna quis erat tempus placerat ut quis nisl. Etiam imperdiet nunc luctus sem hendrerit, ac scelerisque diam cursus. Duis sodales pharetra nunc, sed semper lacus luctus a. Nullam eget fringilla ligula, vel feugiat diam.</p>\r\n', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sit amet sem neque. In id urna quis erat tempus placerat ut quis nisl. Etiam imperdiet nunc luctus sem hendrerit, ac scelerisque diam cursus. Duis sodales pharetra nunc, sed semper lacus luctus a. Nullam eget fringilla ligula, vel feugiat diam.</p>\r\n', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sit amet sem neque. In id urna quis erat tempus placerat ut quis nisl. Etiam imperdiet nunc luctus sem hendrerit, ac scelerisque diam cursus. Duis sodales pharetra nunc, sed semper lacus luctus a. Nullam eget fringilla ligula, vel feugiat diam.</p>\r\n', NULL, '2018-02-09 03:35:00');

-- --------------------------------------------------------

--
-- Estrutura para tabela `projetos`
--

CREATE TABLE `projetos` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `capa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `projetos`
--

INSERT INTO `projetos` (`id`, `ordem`, `slug`, `titulo`, `capa`, `descricao`, `created_at`, `updated_at`) VALUES
(1, 0, 'exemplo', 'Exemplo', '6_20180209033718.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sit amet sem neque. In id urna quis erat tempus placerat ut quis nisl.', '2018-02-09 03:37:18', '2018-02-09 03:37:18');

-- --------------------------------------------------------

--
-- Estrutura para tabela `projetos_imagens`
--

CREATE TABLE `projetos_imagens` (
  `id` int(10) UNSIGNED NOT NULL,
  `projeto_id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `projetos_imagens`
--

INSERT INTO `projetos_imagens` (`id`, `projeto_id`, `ordem`, `imagem`, `created_at`, `updated_at`) VALUES
(1, 1, 0, '2_20180209033728.jpg', '2018-02-09 03:37:29', '2018-02-09 03:37:29'),
(2, 1, 0, '1_20180209033728.jpg', '2018-02-09 03:37:29', '2018-02-09 03:37:29'),
(3, 1, 0, '3_20180209033728.jpg', '2018-02-09 03:37:30', '2018-02-09 03:37:30'),
(4, 1, 0, '4_20180209033729.jpg', '2018-02-09 03:37:30', '2018-02-09 03:37:30'),
(5, 1, 0, '6_20180209033729.jpg', '2018-02-09 03:37:30', '2018-02-09 03:37:30'),
(6, 1, 0, '5_20180209033730.jpg', '2018-02-09 03:37:30', '2018-02-09 03:37:30');

-- --------------------------------------------------------

--
-- Estrutura para tabela `sobre`
--

CREATE TABLE `sobre` (
  `id` int(10) UNSIGNED NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `sobre`
--

INSERT INTO `sobre` (`id`, `imagem`, `texto`, `created_at`, `updated_at`) VALUES
(1, '1_20180209033527.jpg', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sit amet sem neque. In id urna quis erat tempus placerat ut quis nisl. Etiam imperdiet nunc luctus sem hendrerit, ac scelerisque diam cursus. Duis sodales pharetra nunc, sed semper lacus luctus a.</p>\r\n\r\n<p>Nullam eget fringilla ligula, vel feugiat diam. Vestibulum dictum cursus mauris et consequat. Vivamus vitae nisi ac purus rutrum blandit et in nibh. Vestibulum ligula est, consequat ut consequat sed, aliquam at mi. Suspendisse non mattis sem. Proin a rutrum felis. Mauris malesuada sem arcu, et ultrices nunc accumsan ut. Nam venenatis, augue quis tincidunt luctus, felis turpis pharetra dolor, ultricies cursus lorem mi sed lectus. Donec mattis eleifend metus a sollicitudin.</p>\r\n\r\n<p>Vivamus vehicula interdum tristique. Phasellus volutpat faucibus eros, vitae porta felis facilisis vel. Nullam vel ultricies ante.</p>\r\n', NULL, '2018-02-09 03:35:38');

-- --------------------------------------------------------

--
-- Estrutura para tabela `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'trupe', 'contato@trupe.net', '$2y$10$WK6xRaff86iR4us68lKav.i7y6vDdZTsl5pFk/IyVhNFCjCd8RD6q', NULL, NULL, NULL);

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `antes_e_depois`
--
ALTER TABLE `antes_e_depois`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `configuracoes`
--
ALTER TABLE `configuracoes`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `nosso_trabalho`
--
ALTER TABLE `nosso_trabalho`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `projetos`
--
ALTER TABLE `projetos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `projetos_imagens`
--
ALTER TABLE `projetos_imagens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `projetos_imagens_projeto_id_foreign` (`projeto_id`);

--
-- Índices de tabela `sobre`
--
ALTER TABLE `sobre`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `antes_e_depois`
--
ALTER TABLE `antes_e_depois`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `configuracoes`
--
ALTER TABLE `configuracoes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `contato`
--
ALTER TABLE `contato`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `nosso_trabalho`
--
ALTER TABLE `nosso_trabalho`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `projetos`
--
ALTER TABLE `projetos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `projetos_imagens`
--
ALTER TABLE `projetos_imagens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de tabela `sobre`
--
ALTER TABLE `sobre`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `projetos_imagens`
--
ALTER TABLE `projetos_imagens`
  ADD CONSTRAINT `projetos_imagens_projeto_id_foreign` FOREIGN KEY (`projeto_id`) REFERENCES `projetos` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
